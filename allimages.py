#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2022 Dominic Walden
# Copyright (c) 2003-2021 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


# 1. Follow the install instructions for pywikibot:
# https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Copy this file (allimages.py) into the pywikibot "scripts" directory

# Enumerate 10 images from beta commons:
# python3 pwb.py allimages -lang:commons -family:betawikipedia

# Enumerate 100 images from beta commons in descending order:
# python3 pwb.py allimages -l100 -d descending -lang:commons -family:betawikipedia

# Enumerate 10 images from beta commons starting with the image 1929_wall_street_crash_graph_minimal12.svg:
# python3 pwb.py allimages -f "1929_wall_street_crash_graph_minimal12.svg" -lang:commons -family:betawikipedia


import argparse
import pywikibot


class AllImages(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-l', '--limit', default=10)
        parser.add_argument('-d', '--dir', choices=['ascending', 'descending', 'newer', 'older'], default='ascending')
        parser.add_argument('-f', '--aifrom')
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        params = {'action': 'query',
                  'list': 'allimages',
                  'aiprop': 'url|mime',
                  'aidir': self.options.dir,
                  'ailimit': self.options.limit}
        if self.options.aifrom:
            params['aifrom'] = self.options.aifrom
        req = self.site._request(parameters=params)
        images = req.submit()

        filtered_images = []
        for image in images['query']['allimages']:
            if True: # image['mime'] in ["image/jpeg", "image/png"]:
                # TODO: don't hardcode the upload URL
                filtered_images.append(image['url'].replace("https://upload.wikimedia.beta.wmflabs.org/wikipedia/commons/", ""))
        return filtered_images


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = AllImages(*args)
    print(app.run())


if __name__ == '__main__':
    main()
