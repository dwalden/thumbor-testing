#!/bin/bash

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html


# How to run:
# 1. mkdir old new diff
# 2. Start thumbor on localhost port 8800 (or change the url below)
# 3. Run: bash comparison_prototype.sh
# 4. Look at results in comparison_results.csv

# The different metrics used to compare files (e.g. AE, MAE, etc.) are
# documented http://www.imagemagick.org/script/command-line-options.php#metric

first=0

# Taken from https://commons.wikimedia.beta.wmflabs.org/w/api.php?action=query&list=allimages&aiprop=url|mime&ailimit=100
for filepath in '8/8d/%22%22Hotelli_Rukaj%C3%A4rvi%22%22.jpg' 'd/d4/%22Wild%22_Burros.jpg' 'a/a1/%22_Hitler_ja_Mannerheim_%22.jpg' 'a/ad/%25%21-%22%24%26%27%28%29%2A%2C---.--0-9-%3B%3D%3F%40A-Z----%5E_%60a-z~%C2%80%C3%BF.svg' '7/7a/%28Non%29_Conventional_Deposits.svg' 'a/aa/%28Test%29_Forbidden_City_Beijing_Shenwumen_Gate.jpg' 'd/d2/-640px-covid_vaccination.jpg' '0/02/-_8555_%E2%80%93_Scoliopteryx_libatrix_%E2%80%93_Herald_Moth.jpg' 'e/ec/-personShown-.jpg' '4/4e/-tmp-04da30b01ba27b81c1280d9a25b12e53.jpg' '7/7c/-tmp-402b8524d9336e095af71f9b3d28bf8a.jpg' 'b/bd/-tmp-410114c8d20b8479ec055fdeabedf7ab.jpg' '3/3d/-tmp-68731774c0a8ad3538dc8ff33c9615bd.jpg' '5/55/-tmp-7b050823e815b3d5f22d7a073ce190aa.jpg' '3/3f/-tmp-a691b76af36fc5bea8376b91a4c51214.jpg' 'b/b4/-tmp-ac1bcf11b31c0cfbf28892c29b73f7b6.jpg' '7/72/-tmp-b3dd034149f7602e121862a19a7b1d3c.jpg' '3/3c/-tmp-cfecfc5cf0d241751b11d9c2954482ec.jpg' 'd/d5/....jpg' 'b/bb/01-01.png' 'd/d1/01-02.png' '0/03/01._sts098-713a-037.tif' 'b/b4/01._sts098-713a-065.tif' '5/51/01._sts098-713a-089.tif' '9/9b/01.sts098-713a-037.tif' '2/2a/01.sts098-713a-065.tif' 'e/eb/01.sts098-713a-075.tif' '9/91/02-01.png' '4/43/02-03.png' 'e/ee/03-02.png' '4/49/09._sts129-s-119.tif' 'd/d5/0u8gjbi.jpg' '3/30/1-en.png' 'f/f3/1.jpg' 'a/ae/100_Years_War_France_1435-sr.svg' '6/62/1024px-Ba%C3%B1os_de_Vakil%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_36-38_HDR.jpg' 'c/c8/1024x786.png' '7/76/1024x787.png' 'b/b4/1024x788.png' 'a/a2/1024x789.png' 'f/fb/1024x790.png' '9/99/104-10001-10015_01.pdf' 'e/ef/104-10001-10015_02.pdf' '7/7d/104-10001-10015_03.pdf' '4/44/104-10001-10015_04.pdf' '2/2a/104-10001-10015_05.pdf' 'c/c3/104-10001-10015_06.pdf' 'c/cc/104-10001-10015_07.pdf' 'f/f0/10_test_-.jpg' 'f/fb/11%27_Chamber_Testing.jpg' '6/62/11.jpg' '1/11/111.jpg' '2/27/1111145.jpg' 'f/f8/1199px_aspect_ratio_thumbnail_crop_test.png' '1/1d/11_2.jpg' '8/8d/12.jpg' 'c/ce/1212.jpg' 'c/c2/121212.jpg' 'e/e1/1212_2.jpg' 'f/f4/123.jpg' '5/5f/123123123.jpg' '1/16/123123123_2.jpg' '9/99/123123123_3.jpg' 'b/b5/1234_1234_q.foo.JPG' '5/5b/123_-_1.jpeg' 'c/cc/123_-_2.jpeg' 'a/a6/123_-_3.jpeg' '1/14/123_10.jpg' '0/00/123_11.jpg' '4/43/123_12.jpg' '4/42/123_13.jpg' '8/8a/123_14.jpg' 'd/db/123_2.jpg' '4/49/123_3.jpg' 'd/de/123_4.jpg' 'a/ae/123_5.jpg' 'e/e1/123_6.jpg' '9/9b/123_7.jpg' 'b/bc/123_8.jpg' 'd/d7/123_9.jpg' 'b/b1/1280px-D%C3%BClmen%2C_Privatr%C3%B6sterei_Schr%C3%B6er%2C_Kaffeebeh%C3%A4lter_--_2018_--_0529.jpg' '2/23/1397063114285589215464574.jpg' 'f/f1/15-%D1%8B_%D0%BA%D0%BE%D1%80%D0%BF%D1%83%D1%81_%D0%91%D0%9D%D0%A2%D0%A3.jpg' '1/13/1900s_telephone.stl' '0/0e/190788883.png' '1/1b/1915.534.jpg' '3/3f/1916.1044_full.tif' '7/76/1923.04.01Surprise1923ShortDaveFleischerOutOfTheInkwellFilms.ogv' 'e/e3/1928807_507019516737_2545_n.jpg' 'c/c5/1929_wall_street_crash_graph.svg' 'c/cc/1929_wall_street_crash_graph1.svg' '8/8f/1929_wall_street_crash_graph2.svg' '6/62/1929_wall_street_crash_graph_minimal.svg' '5/56/1929_wall_street_crash_graph_minimal1.svg' '7/79/1929_wall_street_crash_graph_minimal10.svg' '6/66/1929_wall_street_crash_graph_minimal11.svg' '1/15/1929_wall_street_crash_graph_minimal12.svg' '7/7b/1929_wall_street_crash_graph_minimal2.svg' '5/5d/1929_wall_street_crash_graph_minimal3.svg' 'f/fa/1929_wall_street_crash_graph_minimal4.svg'
do
    filename=$(basename $filepath)
    # Download the original version of the image
    # wget -nc -P images "https://upload.wikimedia.beta.wmflabs.org/wikipedia/en/"$filepath
    for width in 320 480 640
    do
	# Download the thumbnail image from beta
	wget -nc -O old/$width"px-""$filename" "https://upload.wikimedia.beta.wmflabs.org/wikipedia/commons/thumb/"$filepath"/"$width"px-""$filename"
	# Download the thumbnail image from local thumbor
	wget -nc -O new/$width"px-""$filename" "http://localhost:8800/thumbor/unsafe/"$width"x/https://upload.wikimedia.beta.wmflabs.org/wikipedia/commons/$filepath"

	metrics=""
	metricnames=""
	# We use all the metrics supported by compare v6
	# (http://www.imagemagick.org/script/command-line-options.php#metric)
	for metric in AE MAE FUZZ MEPP MSE NCC PAE PHASH PSNR RMSE
	do
	    metricnames+=",$metric"
	    metrics+=',"'$(compare -metric $metric old/$width"px-""$filename" new/$width"px-""$filename" "diff/"$metric"-"$width"px-""$filename" 2>&1)'"'
	done

	metricnames+=",old width,old height,new width,new height"
	metrics+=","$(identify -ping -format '%w,%h' old/$width"px-"$filename)
	metrics+=","$(identify -ping -format '%w,%h' new/$width"px-"$filename)

	if [ $first -eq 0 ]
	then
	    echo "File,Width$metricnames" > comparison_results.csv
	    first=1
	fi
	echo "$filename,$width$metrics" >> comparison_results.csv
    done
done
